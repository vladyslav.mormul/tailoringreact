import React from "react";
import codeFon from "./check_code_fon.jpg"
import AuthorizationBody from "../../tools/AuthorizationBody";
import "./input_code.css"
import RedirectTelegram from "./RedirectTelegram";
import {useSearchParams} from "react-router-dom";
import {useLoadingPhoneNumber} from "./confirmCodeHooks";
import InputCode from "./InputCode";

function ConfirmCode() {
    const [searchParams, setSearchParams] = useSearchParams();
    const phoneNumber = searchParams.get("phoneNumber")
    const loading = useLoadingPhoneNumber(phoneNumber)

    return (
        <AuthorizationBody img={codeFon}>
            <div className={'confirm-code-container'}>
                <form className="confirm-code-form">
                    {
                        loading ?
                            <RedirectTelegram phoneNumber={phoneNumber}/> :
                            <InputCode phoneNumber={phoneNumber}/>
                    }
                </form>
            </div>


        </AuthorizationBody>
    )
}

export default ConfirmCode