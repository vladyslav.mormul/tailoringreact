import React, {useState} from "react";
import {useInputCode} from "./confirmCodeHooks";

function InputCode({phoneNumber}) {

    const [codeError, sendCode, setInputCode] = useInputCode(phoneNumber)

    return (
        <>
            <p> Enter the confirmation code that you received on Telegram </p>
            {
                codeError ?
                    <em id="code-incorrect-text-id">Code incorrect</em> :
                    <></>
            }

            <input type="text" name="" id="code-value-id" placeholder="Code here" maxLength="20" minLength={20}
                   onChange={(e) => setInputCode(e)}/>
            <button className={'telegram-link-button'} type="button" onClick={() => sendCode()}>Check</button>
        </>
    )
}

export default InputCode