import React from "react";

function RedirectTelegram({phoneNumber = 'Phone number not found'}) {

    return (
        <>
            <p>Confirm your phone number with Telegram bot. Your number is <strong
            >{phoneNumber}</strong>
            </p>

            <a href="https://t.me/needlework_number_bot" target="_blank" rel="noopener noreferrer"
               className={'telegram-link-button'}>Telegram bot</a>
        </>
    )
}

export default RedirectTelegram