import {useCallback, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import confirmCode from "./ConfirmCode";

const CHECK_PHONE_NUMBER_URL = 'http://localhost:8100/authorization/number';
const CONFIRM_CODE_URL = 'http://localhost:8100/authorization/confirm-code';
const REGEX_CODE = new RegExp('[0-9a-zA-Z]{20,20}');
const TEN_SECONDS = 10000;

async function pinkPhoneNumber(phoneNumber) {
    const respond = await fetch(CHECK_PHONE_NUMBER_URL + '?number=' + phoneNumber);
    if (respond.status > 299) {
        alert('Please reload the page. ' + await respond.text());
        return false;
    }

    return await respond.json();
}

function useLoadingPhoneNumber(phoneNumber) {
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const interval = setInterval(() => {
            if (loading) {
                pinkPhoneNumber(phoneNumber)
                    .then(answer => setLoading(!answer))
            }
        }, TEN_SECONDS);
        return () => clearInterval(interval);
    }, [loading, phoneNumber]);

    return loading
}

function useInputCode(phoneNumber) {
    const [codeError, setCodeError] = useState(false)
    const [code, setCode] = useState("")
    const navigate = useNavigate()

    const sendCode = useCallback(async () => {
        const isMatch = REGEX_CODE.test(code)
        const sentCode = await sendCodeForConfirm(code, phoneNumber)

        if (isMatch && sentCode) {
            navigate("../login", {replace: true})
        }
        setCodeError(true);
    }, [code, navigate, phoneNumber])


    const setInputCode = (e) => {
        setCode(e.target.value)
    }

    return [codeError, sendCode, setInputCode]
}

async function sendCodeForConfirm(code, phoneNumber) {
    const respond = await fetch(CONFIRM_CODE_URL, {
        method: 'POST',
        body: JSON.stringify({
            phoneNumber, code
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })

    const respondDate = respond.status < 299 && await respond.json()

    if (!respondDate) {
        const errors = await respond.json()
        for (const error of errors) {
            console.log(`${error.param} = ${error.msg}`)
        }
        return false
    }

    return true
}

export {useLoadingPhoneNumber, useInputCode}