import styles from './between.input.style.module.css'

const BetweenInput = ({right, left, label, labelText}) => {
    return (
        <div className={styles.betweenInputContainer}>
            <label {...label}>{labelText}</label>
            <div className={styles.betweenInputContainerTwoInputs}>
                <input {...right}/>
                <input {...left}/>
            </div>
        </div>
    )
}

export default BetweenInput