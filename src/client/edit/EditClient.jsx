import styles from './edit.client.module.css'
import BeautifulRadio from "../../tools/beautiful.radio/BeautifulRadio";
import ChooseLocation from "../../location/ChooseLocation";
import {useContext, useEffect, useState} from "react";
import {AuthContext} from "../../authorization/context/AuthContext";
import {toTittleCase} from "../../tools/stringTools";
import Modal from "../../tools/modal/Modal";
import Story from "./Story";

const EditClient = () => {
    const {user, token, login} = useContext(AuthContext)
    const [isOpenStory, setOpenStory] = useState(false)
    const [stories, setStories] = useState([])
    const [editUser, setEditUser] = useState(() => toEditUser(user))

    useEffect(() => {
        setEditUser(toEditUser(user))
    }, [user])

    const handleInput = (name) => {
        return {
            placeholder: toTittleCase(name),
            value: editUser[name],
            onChange: ({target}) => setEditUser(prev => {
                return {...prev, [name]: target.value}
            })
        }
    }

    const changeGender = (v) => {
        setEditUser(prev => {
            return {...prev, male: v}
        })
    }

    const showStory = () => {
        setOpenStory(true)
    }

    const findStories = async () => {
        const respond = await fetch("http://localhost:8100/users/mementos", {
            headers: {
                'Authorization': 'Bearer ' + (token() ?? ''),
            },
        })

        if (respond.ok) {
            const data = await respond.json();
            return setStories(data)
        }
        alert(await respond.text())
    }

    useEffect(() => {
        findStories()
    }, [])

    const useEdit = async () => {
        const formDate = new FormData()
        for (const key of Object.keys(editUser)) {
            if (user[key] !== editUser[key]) {
                formDate.append(key, editUser[key])
            }

        }

        const respond = await fetch('http://localhost:8100/users', {
            headers: {
                'Authorization': 'Bearer ' + (token() ?? ''),

            },
            body: formDate,
            method: 'PUT'
        })

        if (!respond.ok) {
            return alert('Error edit')
        }

        const data = await respond.json();
        if (data?.token) {
            await findStories()
            login(data.token, data.user, data.endTokenDate)
        }
        console.log(data)
    }

    return (
        <div className={styles.container}>
            <div className={styles.editBox}>
                <h2>Edit</h2>
                <div className={styles.editInputContainer}>
                    <input {...handleInput('firstname')}/>
                    <input {...handleInput('lastname')}/>
                    <input {...handleInput('country')}/>
                    <input {...handleInput('city')}/>
                    <input {...handleInput('email')}/>
                    <BeautifulRadio setValue={changeGender} firstText={'Male'} secondText={'Female'} name={'male'}
                                    defaultValue={editUser.male}/>
                    <button className={[styles.editButton, styles.active].join(' ')}>Edit phone number</button>
                    <button className={styles.editButton} onClick={showStory}>Show story</button>
                </div>
                <div className={styles.confirmEditButtonContainer}>
                    <button type={'button'} onClick={useEdit}>Edit</button>
                </div>
            </div>
            <Modal active={isOpenStory} setActive={setOpenStory}>
                {
                    stories.map((s) => <Story story={s} key={`story=${s.id}`}/>)
                }
            </Modal>
        </div>
    )
}

export default EditClient

function toEditUser(user) {
    return {
        firstname: user.firstname,
        lastname: user.lastname,
        city: user.city,
        country: user.country,
        email: user.email,
        male: user.male
    }
}
