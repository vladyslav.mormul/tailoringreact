import styles from './story.module.css'
import {useContext, useState} from "react";
import {AuthContext} from "../../authorization/context/AuthContext";

const Story = ({story}) => {
    const {token, user, login} = useContext(AuthContext)
    const [disableButton, setDisableButton] = useState(false)
    const useStory = async () => {
        setDisableButton(true)
        const formData = new FormData()
        formData.append('id', story.id)
        const respond = await fetch('http://localhost:8100/users/mementos', {
            headers: {
                'Authorization': 'Bearer ' + (token() ?? ''),
            },
            method: 'PUT',
            body: formData
        })

        setDisableButton(false)
        if (!respond.ok) {
            return alert(respond.text())
        }

        const data = await respond.json()
        alert('Restore ' + (data ? 'Successful' : 'Unsuccessful'))
        if (data?.token) {
            login(data.token, data.user, data.endTokenDate)
        }
    }

    return (
        <div className={styles.box}>
            <div className={styles.container}>
                <p>Where : <em>{story.dateOfCreation}</em></p>

                <button disabled={disableButton} onClick={useStory}>Use</button>
            </div>
            <details>
                <summary>Show changes</summary>
                {
                    Object.keys(story.state).filter(n => story.state[n]).map((n, i) =>
                        <p key={`story--${story.id}-date-${i}`}>{n} : <samp>{"" + story.state[n]}</samp></p>)
                }
            </details>
        </div>
    )
}

export default Story